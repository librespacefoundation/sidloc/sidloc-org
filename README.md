# SIDLOC
SIDLOC (Spacecraft Identification and Localization) is a proposed standard for satellite and spacecraft identification and localization.
It is developed as an [ESA](https://esa.int) funded, [ARTES](https://artes.esa.int) activity by [Libre Space Foundation](https://libre.space) and [FORTH](https://www.forth.gr).

This is the organizational repository of the SIDLOC project.
We use the Gitalb issue tracking features to organize and manage the development of the SIDLOC, using modern and agile techniques.

# Working Groups
## WG0: Modulation, coding and framing design
### Tasks
* Literature review for DSSS and variations
* Simulations
* Link budget
* Frequency offset performance analysis
### Related repositories
* N/A 
### Leader: @surligas
### Members: @surligas, @zisi, @aris12, @acinonyx 

  
## WG1: RF frontend design
### Tasks
* Generate DSSS signals (feedback from WG0)
* Amplification
* Filtering and conformance with the SFCG 21RG
### Related repositories
* N/A 
### Leader: @zisi  
### Members: @zisi, @surligas, @deligeo, @acinonyx, @aris12

## WG2: Digital and Power management design
### Tasks
* Power budget analysis
* On board power management
* Multiple power sources
* Power
### Related repositories
* N/A 
### Leader: @drid  
### Members: @drid, @acinonyx, @papamat 

## WG3: Ground station support
### Tasks
* Analysis of the number of sats at the field of view of the GS (give feedback on the WG0)
* Performance analysis on the STRF (give feedback on the WG0)
* Scheduling on the GS
* Proof of concept DSSS spreading sequence assignment
### Related repositories
* N/A 
### Leader: @adamkalis  
### Members: @adamkalis, @surligas, @acinonyx, @papamat 

## WG4: Software
### Tasks
* Implement findings of WG0 on the beacon
* Implement findings of WG0 on the GS
* Implement drivers for the beacon ICs
* Implement/improve TLE estimation using multiple sources
* Implement OBC routines on the beacon
### Related repositories
* N/A 
### Leader: @surligas 
### Members: @surligas, @drid, @acinonyx, @adamkalis 

## WG5: Antenna
### Tasks
* Performance analysis for the antenna type
### Related repositories
* N/A 
### Leader: @aris12
### Members: @aris12, @acinonyx, @deligeo, @papamat 

## WG6: Mechanical
### Tasks
* Antenna deployment 
* Mounting
* Thermal
* Electrical interface
### Related repositories
* N/A 
### Leader: @papamat 
### Members: @papamat, @acinonyx, @drid, @aris12    
